/* Probar el Jquery 
$(document).ready(function(){
  alert('Jquery Perfecto')  
}); 

function obtenerData(){
    var porNombre = document.getElementsByName("nombres")[0].value;
    alert(porNombre);
} 

sin jQuery

function obtenerData(){
    var data = document.getElementsByName('nombres')[0].value;
    document.getElementById('esc').innerHTML=data;
} 

con jQuery

function obtenerData(data){
    $('#esc').html(data);
}*/

function obtenerData(data){
    $.ajax({
        //Protocolo HTTP
        type:'post',
        //URL desno (donde se envian los datos que el usuario digita(controller))
        url:'?c=usuarios&m=response_ajax',
        //Los datos que enviamos por la peticion ejemplo (data)
        data:{data:data},
        //Respuesta del proceso con su parametro de respuesta (esc) => die($e->getMessage())
        success(esc){
            $('#response_ajax').html(esc);
        }
    })
}