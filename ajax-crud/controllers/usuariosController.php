<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DB
 *
 * @author Henderson Moreno
 */
 
class UsuariosController
{

    private $modelUsuario;

    public function __construct()
    {
        try {
            $this->modelUsuario = new Usuario();
        } catch (Exception $e) {
            die($e->getMessage());
        };
    }

    public function home()
    {
        require_once('views/usuario/home.php');
    }

    public function response_ajax()
    {
        $data = '%' . $_REQUEST['data'] . '%';
        foreach ($this->modelUsuario->consultar_like($data) as $value) {
            ?>
        <p><?php echo $value->nombres ?></p>
    <?php
}
}
}

?>