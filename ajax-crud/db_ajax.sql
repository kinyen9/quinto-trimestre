-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-04-2019 a las 13:52:05
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_ajax`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombres` varchar(75) COLLATE utf32_unicode_ci NOT NULL,
  `apellidos` varchar(75) COLLATE utf32_unicode_ci NOT NULL,
  `correo` varchar(75) COLLATE utf32_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf32_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombres`, `apellidos`, `correo`, `descripcion`) VALUES
(1, 'Hento', 'Moreno', 'MOreno@jeje', 'HOla como estas'),
(2, 'Hehehe', 'Lololol', 'holacomo@holacomo', 'hehehehe'),
(3, 'Larala', 'Lalala', 'A@jajaja', 'xDDDD'),
(4, 'Jorge', 'Isacas', 'Broma jojo', 'Mi mama me mima'),
(5, 'Antonio', 'De la cases', 'JoJo@xD', 'trololol'),
(6, 'Hento', 'Moreno', 'MOreno@jeje', 'HOla como estas'),
(7, 'Hehehe', 'Lololol', 'holacomo@holacomo', 'hehehehe'),
(8, 'Larala', 'Lalala', 'A@jajaja', 'xDDDD'),
(9, 'Jorge', 'Isacas', 'Broma jojo', 'Mi mama me mima'),
(10, 'Antonio', 'De la cases', 'JoJo@xD', 'trololol');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
