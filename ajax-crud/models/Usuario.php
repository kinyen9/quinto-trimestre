<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Henderson Moreno
 */
class Usuario
{

    /*Pdo Guarda la conexion a la DB */

    private $pdo;

    public function __construct()
    {
        try {
            $this->pdo = DB::Conectar();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function crear()
    { }

    public function consultar()
    { }

    public function consultar_id()
    { }

    public function consultar_like($data)
    {
        try {
            $stm = $this->pdo->prepare("SELECT * FROM usuarios WHERE nombres LIKE ?");
            $stm->bindParam(1, $data, PDO::FETCH_STR);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
