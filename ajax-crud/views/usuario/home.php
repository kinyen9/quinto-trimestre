<html>

<head>
    <title>Ajax</title>
</head>

<body>
    <label for="nombres">Nombres</label>
    <input id="nombres" name="nombres" placeholder="Escriba un nombre" onkeyup="obtenerData(this.value)">
    <div id="response_ajax">

    </div>
    <script src="assets/jquery.js"></script>
    <script src="assets/app.js"></script>
</body>

</html>