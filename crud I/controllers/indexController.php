<?php  

	class indexController{ //se crea la clase

		private $modelUsuario;

			public function __construct(){
				try {
					$this->modelUsuario= new Usuario();
				} catch (Exception $e) {
					die($e->getMessage());
				}
			}

		public function index() { //se crea el metodo
			require_once('views/all/header.php'); // hace un llamado a lo que se encuentra en el header de la carpeta views
			require_once('views/index.php'); // hace un llamado a lo que se encuentra en el index de la carpeta views
			require_once('views/all/footer.php'); // hace un llamado a lo que se encuentra en el footer de la carpeta views
		}

		public function registrarUsuario(){
			$documento=$_POST['documento'];
			$nombre=$_POST['nombre'];
			$apellido=$_POST['apellido'];
			$correo=$_POST['correo'];
			$this->modelUsuario->crear($documento,$nombre,$apellido,$correo);
			header("location:?c=index&m=index");
		}

		public function eliminarUsuario(){
			$idusuario=$_POST['idusuario'];
			$this->modelUsuario->eliminar($idusuario);
			header("location:?c=index&m=index");
		}
		public function editarUsuario(){
			require_once('views/all/header.php');
			require_once('views/editar.php');
			require_once('views/all/footer.php');
		}

		public function actualizarUsuario(){
			$documento=$_POST['documento'];
			$nombre=$_POST['nombre'];
			$apellido=$_POST['apellido'];
			$correo=$_POST['correo'];
			$idusuario=$_POST['idusuario'];
			$this->modelUsuario->actualizar($documento,$nombre,$apellido,$correo,$idusuario);
			header("location:?c=index&m=index");
		}
	}
?>