<?php 

require_once ('core/core.php');

	if (empty($_REQUEST['c'])){
		$controller='index'; // Esta linea hace que una variablle adopte el index
		require_once ('controllers/'.$controller.'controller.php');// se concatena la variable anterior con la carpeta controller y el archivo controller.php
		$controller=$controller.'controller';//Se sobrecarga la variable con los valores anteriores
		$controller=new $controller();//ahora se crea un objeto controller nuevo que llama a la clase indexController del archivo indexcontroller.php
		$controller->index(); // el apuntador del objeto creado dice que debe iniciar en el metodo index con las funciones requeridas en indexController.php

		/*Las cuatro primeras lineas hacen un llamado similar para que el sistema realice la conexion del usuario*/
		}else{
		
		$controller=$_REQUEST['c'];
		require_once ('controllers/'.$controller.'controller.php');
		$controller=$controller.'controller';
		$controller=new $controller();
		$metodo= isset($_REQUEST['m']) ? $_REQUEST['m'] : 'index';// se define el metodo de acuerdo a lo que el cliente va a solicitar en los menu
		call_user_func(array($controller,$metodo));// las llamadas del usuario se establecen para el controlador y los metodos
	}
 ?>