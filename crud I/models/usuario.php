<?php 

class Usuario {

	private $pdo;

	public function __construct(){
		try {
			$this->pdo= Database::Conectar();
		} catch (Exception $e) {
			die ($e->getMessage());
		}
	}

	public function consultar(){
		try {
			
			$stm=$this->pdo->prepare("SELECT * FROM usuarios");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die ($e->getMessage());
			
		}

	}

	public function crear($documento,$nombre,$apellido,$correo){
		try {
			$stm=$this->pdo->prepare("INSERT INTO usuarios(documento,nombre,apellido,correo) VALUES(?,?,?,?)");
			$stm->bindparam(1,$documento,PDO::PARAM_STR);
			$stm->bindparam(2,$nombre,PDO::PARAM_STR);
			$stm->bindparam(3,$apellido,PDO::PARAM_STR);
			$stm->bindparam(4,$correo,PDO::PARAM_STR);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function actualizar($documento,$nombre,$apellido,$correo,$idusuario){
		try {
			$stm=$this->pdo->prepare("UPDATE usuarios SET documento = ?, nombre = ?, apellido = ?, correo = ? WHERE idusuario = ?");
			$stm->bindparam(1,$documento,PDO::PARAM_STR);
			$stm->bindparam(2,$nombre,PDO::PARAM_STR);
			$stm->bindparam(3,$apellido,PDO::PARAM_STR);
			$stm->bindparam(4,$correo,PDO::PARAM_STR);
			$stm->bindParam(5,$idusuario,PDO::PARAM_INT);
			$stm->execute();
			} catch (Exception $e) {
				die($e->getMessage());
			}		
	}

	public function eliminar($idusuario){
		try {
			$stm=$this->pdo->prepare("DELETE FROM usuarios WHERE idusuario = ?");
			$stm->bindParam(1,$idusuario,PDO::PARAM_INT);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function consultar_id($idusuario){
		try {
			$stm=$this->pdo->prepare("SELECT * FROM usuarios WHERE idusuario = ?");
			$stm->bindParam(1,$idusuario,PDO::PARAM_INT);
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

}

 ?>