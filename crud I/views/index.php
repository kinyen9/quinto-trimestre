<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="table-responsive">
				<table class="table table-bordered table-condensed">
					<tr>
						<th>Documentos</th>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Correo</th>
						<th>Fecha de Creacion</th>
						<th>Editar</th>
						<th>Eliminar</th>
					</tr>
					<?php foreach ($this->modelUsuario->consultar() as $value) {
						?>
					<tr>
						<td><?php echo $value->documento ?> </td>
						<td><?php echo $value->nombre ?></td>
						<td><?php echo $value->apellido ?> </td>
						<td><?php echo $value->correo ?></td>
						<td><?php echo $value->created_ad ?></td>
						<td><form action="?c=index&m=editarUsuario" method="POST">
								<input type="hidden" name="idusuario" id="idusuario" value="<?php echo $value->idusuario ?>">
								<button class="btn btn-info btn-sm">Editar</td>
							</form>	
						<td><form action="?c=index&m=eliminarUsuario" method="POST">
								<input type="hidden" name="idusuario" id="idusuario" value="<?php echo $value->idusuario ?>">
								<button class="btn btn-danger btn-sm">Eliminar</td>
							</form>
						</td>
					</tr>
					<?php 	}
						?>
				</table>
			</div>
		</div>

		<div class="col-md-4">
			<form action="?c=index&m=registrarUsuario" method="POST">
				<div class="form-group">
					<label for="documento">Documento</label>
					<input type="text" name="documento" id="documento" class="form-control">
				</div>

				<div class="form-group">
					<label for="nombre">Nombres</label>
					<input type="text" name="nombre" id="nombre" class="form-control">
				</div>
						
				<div class="form-group">
					<label for="apellido">Apellidos</label>
					<input type="text" name="apellido" id="apellido" class="form-control">
				</div>
						
				<div class="form-group">
					<label for="correo">Correo</label>
					<input type="email" name="correo" id="correo" class="form-control">
				</div>
				<hr>
				<button class="btn btn-info">Registrar</button>
			</form>

		</div>
	</div>
</div>