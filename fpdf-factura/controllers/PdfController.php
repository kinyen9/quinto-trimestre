<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PdfController
 *
 * @author Henderson Moreno
 */

require_once 'libs/fpdf/fpdf.php'; // aca se solicita la ruta donde se encuentra la libreria fPdf (se tuvo que crear un folder previamente)

class PdfController {
    
    private $modelProducto;
    
    public function __construct() {
        try {
            $this->modelProducto = new Producto();    
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }
    
    public function factura(){
        foreach ($this->modelProducto->consultar_id($_REQUEST['id']) as $value){}
        /*  Metodos FPDF
        Cell(ancho,alto, codificacion('contenido'),bordes,bordes,'posicion');
        AddPage(tipo[COURIER, HELVETICA, ARIAL, TIMES, SYMBOL, ZAPDINGBATS], estilos[normal, B, I, U], tamaño);
        Cell(ancho, alto, texto, bordes, ?, alineacion, rellenar, link);
        Ln() -- Hace un salto entre linea de texto;
        OutPut(destino[],nombre_archivo, utf8);*/
        
        $pdf = new FPDF('p','mm','letter');
        $pdf->SetMargins(20,25,20);
        $pdf->SetFont('Arial','B',12);
        $pdf->AddPage();
        $pdf->Cell(180,10, utf8_decode('Centro de Electricidad, Electronica y Telecomunicaciones'),1,null,'C');
        $pdf->Ln(20);
        $pdf->Cell(40,10, utf8_decode('Número Producto'),1,null,'C');
        $pdf->Cell(75,10, utf8_decode('Producto'),1,null,'C');
        $pdf->Cell(28,10, utf8_decode('Descripción'),1,null,'C');
        $pdf->Cell(17,10, utf8_decode('Precio'),1,null,'C');
        $pdf->Cell(20,10, utf8_decode('Creado'),1,null,'C');
        $pdf->SetFont('Arial','',10);
        $pdf->Ln(11);
        $pdf->Cell(40,10, $value->id_productos,1,null,'C');
        $pdf->Cell(75,10, $value->nombre,1,null,'C');
        $pdf->Cell(28,10, $value->descripcion,1,null,'C');
        $pdf->Cell(17,10, $value->precio,1,null,'C');
        $pdf->Cell(20,10, $value->created,1,null,'C');
        $pdf->Output();
    }
}