<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductosController
 *
 * @author Henderson Moreno
 */
class ProductosController {
    
    private $modelUsuario;

    public function __construct(){
        try {
            $this->modelUsuario = new Producto();
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }
    
    public function home() {
        require_once 'views/all/header.php';
        require_once 'views/producto/home.php';
        require_once 'views/all/footer.php';
    }
    
}
