-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-03-2019 a las 03:44:55
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_factura`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_productos` int(11) NOT NULL,
  `nombre` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `precio` decimal(11,2) DEFAULT NULL,
  `img_factura` text COLLATE utf8_unicode_ci,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_productos`, `nombre`, `descripcion`, `precio`, `img_factura`, `created`) VALUES
(1, 'Maiz', 'planta de maiz grande', '2000.00', NULL, '2019-03-28 01:58:50'),
(2, 'Papas', 'papas fritas', '3000.00', NULL, '2019-03-28 01:58:50'),
(3, 'Carne', 'carne con mucho gordo', '3000.00', NULL, '2019-03-28 23:23:18'),
(4, 'Anfetaminas', 'pegate tu viaje', '400000.00', NULL, '2019-03-28 01:58:50'),
(5, 'Cocaina', 'ojo con la poli', '500000.00', NULL, '2019-03-28 01:58:50'),
(6, 'Boxer', 'que chirrete', '55555.00', NULL, '2019-03-28 01:58:50'),
(7, 'Heroina', 'ufffff', '40000.00', NULL, '2019-03-28 01:58:50'),
(8, 'Crucifijos', 'va de retro satan', '300000.00', NULL, '2019-03-28 01:58:50'),
(9, 'Catequesis', 'jajajajajaja', '400000.00', NULL, '2019-03-28 01:58:50'),
(10, 'Bebes', 'se hacen info aqui', '540000.00', NULL, '2019-03-28 01:58:50');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_productos`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_productos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
