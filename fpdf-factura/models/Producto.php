<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Producto
 *
 * @author Manelauft & Henderson Moreno
 */
class Producto {
    
    private $pdo;
    
    public function __construct() {
        try {
            $this->pdo= DataBase::Conectar();
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }
    
    public function consultar(){
        try {
            $stm=$this->pdo->prepare("SELECT * FROM productos");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }

    public function consultar_id($id_productos){
        try {
            $stm=$this->pdo->prepare("SELECT * FROM Productos WHERE id_productos = ?");
            $stm->bindParam(1,$id_productos,PDO::PARAM_INT);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }

}
?>