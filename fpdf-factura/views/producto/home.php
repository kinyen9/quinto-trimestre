<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-sm table-striped">
                <tr>
                    <th>Numero Producto</th>
                    <th>Producto</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Creado</th>
                    <th>Descargar</th>       
                </tr>
                <?php 
                    foreach ($this->modelUsuario->consultar() as $value) {
                 ?>
                <tr>
                    <td><?php echo $value->id_productos ?></td>
                    <td><?php echo $value->nombre ?></td>
                    <td><?php echo $value->descripcion ?></td>
                    <td><?php echo $value->precio ?></td>
                    <td><?php echo $value->created ?></td>
                    <td><form action="?c=Pdf&m=factura&id=<?php echo $value->id_productos?>" method="POST">
                        <input type="hidden" name="id_productos" id="id_productos" value="<?php echo $value->id_productos ?>">
                        <button value="PDF" class="btn btn-warning btn-sm">Descargar PDF</form></td>
                </tr>
                <?php } ?>
            </table>
            <form action="?c=Pdf&m=factura" method="POST">
                <button value="PDF" class="btn btn-info">Ir a PDF</button>
            </form>
        </div>
    </div>
</div>