<?php

/* 
 * En este controlador se realizara la creacion de 
 * los metodos de autentificacion del usuario con rol Tendero
 * @autor Henderson Moreno
 */

class indexController {
    
    public function login(){
        require_once 'views/all/header.php';
        require_once 'views/index/login.php';
        require_once 'views/all/footer.php';
    }
}

?>