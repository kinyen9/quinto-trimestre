<?php

/* 
 * Este controlador se realizar permitira
 * @autor Henderson Moreno
 */

require_once './assets/libs/fpdf/fpdf.php';

class TenderoController{
    
    public function home(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/home.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function stock(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/stock.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function clientes(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/usuarios.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function perfil(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/perfil.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function registrarUsuario(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/registrarUsuario.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function registrarProducto(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/registrarProducto.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function who(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/who.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function validar(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/ventas/validarCliente.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }
    
    public function escoger(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/tendero/ventas/escogerProducto.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }

    public function facturar(){
        $pdf = new FPDF('p','mm','letter');
        $pdf->AddPage();
        $pdf->Image('assets/img/logo_tienda.png',30,10,-300);
        $pdf->SetFont('Arial','B',12);
        $pdf->SetXY(100, 20);
        $cliente="Número identificación: 132456897 \nCliente: Pepito Peréz \nCorreo Electronico: Pepe@gmail.com";
        $pdf->MultiCell(90,10,utf8_decode($cliente),1,'L');
        $pdf->SetXY(40, 60);
        $pdf->Cell(140,15,utf8_decode('Factura número: 546654 de Fecha: 09 Abril del 2019'),1,0,"C");
        $pdf->SetXY(30, 80);
        $pdf->SetFillColor(0,0,0);
        $pdf->SetTextColor(255,193,7);
        $pdf->Cell(80,10,"Articulo",1,0,"C",true);
        $pdf->Cell(30,10,"Cant.",1,0,"C",true);
        $pdf->Cell(20,10,"$/Ud",1,0,"C",true);
        $pdf->Cell(30,10,"Subt.",1,1,"C",true);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetX(30);
        $pdf->Cell(80,10,'Espinaca China',1,0,"L");
        $pdf->Cell(30,10,'1',1,0,"C");
        $pdf->Cell(20,10,'$'.number_format(6000,2),1,0,"C");
        $pdf->Cell(30,10,'$'.number_format((6000*1),2),1,1,"R");
        $pdf->SetX(30);
        $pdf->Cell(80,10,'Leche',1,0,"L");
        $pdf->Cell(30,10,'1',1,0,"C");
        $pdf->Cell(20,10,'$'.number_format(2400,2),1,0,"C");
        $pdf->Cell(30,10,'$'.number_format((2400*1),2),1,1,"R");
        $pdf->SetX(30);
        $pdf->Cell(80,10,'Chocolate',1,0,"L");
        $pdf->Cell(30,10,'1',1,0,"C");
        $pdf->Cell(20,10,'$'.number_format(8000,2),1,0,"C");
        $pdf->Cell(30,10,'$'.number_format((8000*1),2),1,1,"R");
        $pdf->SetX(30);
        $pdf->Cell(80,10,'Granola',1,0,"L");
        $pdf->Cell(30,10,'2',1,0,"C");
        $pdf->Cell(20,10,'$'.number_format(3500,2),1,0,"C");
        $pdf->Cell(30,10,'$'.number_format((3500*2),2),1,1,"R");
        $pdf->SetX(30);
        $pdf->Cell(80,10,'Tomate',1,0,"L");
        $pdf->Cell(30,10,'3',1,0,"C");
        $pdf->Cell(20,10,'$'.number_format(2000,2),1,0,"C");
        $pdf->Cell(30,10,'$'.number_format((2000*3),2),1,1,"R");
        $pdf->SetX(110);
        $pdf->SetTextColor(255,193,7);
        $pdf->SetFillColor(0,0,0);
        $pdf->Cell(50,10,"Subtotal:",1,0,"C",true);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(30,10,number_format(29400,2),1,1,"R");
        $pdf->SetX(110);
        $pdf->SetTextColor(255,193,7);
        $pdf->Cell(50,10,"IVA (4%): ",1,0,"C",true);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(30,10,number_format(0.04*29400,2),1,1,"R");
        $pdf->SetX(110);
        $pdf->SetTextColor(255,193,7);
        $pdf->Cell(50,10,"Total:",1,0,"C",true);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(30,10,number_format((0.04*29400)+29400,2),1,1,"R");
        $pdf->Output();
    }

}

?>