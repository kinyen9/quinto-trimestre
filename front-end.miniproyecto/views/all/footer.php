<?php
    if($_REQUEST['m'] != 'login'){
?>   
    <footer class="app-footer">
      <div>
        <a href="https://www.amazon.com/">Amazon</a>
        <span>&copy; 2019 A to Z.</span>
      </div>
      <div class="ml-auto">
        <span>Distribuido por</span>
        <a href="https://www.amazon.com/">Amazon</a>
      </div>
    </footer>
<?php
    }
?>
    <!-- CoreUI and necessary plugins-->
    <script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/node_modules/pace-progress/pace.min.js"></script>
    <script src="assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="assets/node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
    <!-- Plugins and scripts required by this view-->
    <script src="assets/node_modules/chart.js/dist/Chart.min.js"></script>
    <script src="assets/node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js"></script>
    <script src="assets/js/main.js"></script>
<?php
    if($_REQUEST['m'] == 'registrarUsuario' || 
    $_REQUEST['m'] == 'registrarProducto' || 
    $_REQUEST['m'] == 'validar' || 
    $_REQUEST['m'] == 'stock'){
?>
    <script src="assets/js/myscript.js"></script>
<?php
    }
?>
  </body>
</html>