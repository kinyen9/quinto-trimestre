    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="?c=tendero&m=home">
          <img class="navbar-brand-full" src="assets/img/logo_tienda.png" alt="Tienda Logo" width="89" height="80" >
          <img class="navbar-brand-minimized" src="assets/img/logito_tienda.png" alt="Tienda Logo Peque" width="30" height="30">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav d-md-down-none">
        <li class="nav-item px-4">
          <a class="nav-link" href="?c=tendero&m=home">Tienda</a>
        </li>
        <li class="nav-item px-4">
          <a class="nav-link" href="?c=tendero&m=stock">Inventario</a>
        </li>
        <li class="nav-item px-4">
          <a class="nav-link" href="?c=tendero&m=validar">Vender</a>
        </li>
      </ul>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown d-md-down-none">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Mona Chimba
          </a>
          <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-header text-center">
                <strong>Opciones</strong>
              </div>
              <a class="dropdown-item" href="?c=tendero&m=perfil">
                <i class="fa fa-user"></i> Perfil</a>
              <a class="dropdown-item" href="?c=index&m=login">
                <i class="fa fa-lock"></i> Cerrar Sessión</a>
            </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="img-avatar" src="assets/img/foto-perfil.jpg" alt="iisdoneii@misena.edu.co">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Opciones</strong>
            </div>
            <a class="dropdown-item" href="?c=tendero&m=perfil">
              <i class="fa fa-user"></i> Perfil</a>
            <a class="dropdown-item" href="?c=index&m=login">
              <i class="fa fa-lock"></i> Cerrar Sessión</a>
          </div>
        </li>
      </ul>
    </header>