<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-title">Consultar</li>
        <li class="nav-item">
          <a class="nav-link" href="?c=tendero&m=perfil">
            <i class="nav-icon icon-note"></i> Perfil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?c=tendero&m=clientes">
            <i class="nav-icon icon-energy"></i> Clientes</a>
        </li>
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle">
            <i class="nav-icon icon-star"></i> Tienda</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="?c=tendero&m=stock">
                <i class="nav-icon icon-pencil"></i> Inventario</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="?c=tendero&m=who">
                <i class="nav-icon icon-bell"></i> Datos Basicos</a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>