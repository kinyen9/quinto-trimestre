  <div class="container cl">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card-group">
          <div class="card p-4">
            <div class="card-body">
              <h1>Ingresar</h1>
              <p class="text-muted">Accede a tu cuenta</p>
              <p>Usuario</p>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-user"></i>
                  </span>
                </div>
                <input class="form-control" type="text" placeholder="MonaArtist" readonly />
              </div>
              <p>Contraseña</p>
              <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-lock"></i>
                  </span>
                </div>
                <input class="form-control" type="password" placeholder="&#11044; &#11044; &#11044; &#11044; &#11044; &#11044; &#11044; &#11044;" readonly />
              </div>
              <div class="row">
                <div class="col-6">
                  <a href="?c=tendero&m=home" class="btn btn-primary px-4" type="button">Acceder</a>
                </div>
                <div class="col-6 text-right">
                  <a href="" class="btn btn-link px-0" type="button" data-toggle="modal" data-target="#myModal">Olvidaste la contraseña?</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 logito d-none d-lg-block">
        <div class="card-group">
          <div class="jumbotron">
            <h2 class="display-3">-Esperando? <span class="display-4">♫ Vende con nosotros</span></h2>
            <p class="lead">
              Aca puedes vender todo lo que tu quieras
            </p>
            <p class="lead">
              <button class="btn btn-block btn-warning" type="button" data-toggle="modal" data-target="#warningModal">Leer más</button>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dark" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Recuperar Contraseña</h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <input class="form-control" type="email" placeholder="MonaArtist@gmail.com" readonly />
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Enviar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-warning" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title miven">Nuestra Tienda</h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Possimus, dolore modi nemo id placeat iure, optio ipsa quae doloremque dolorem eveniet assumenda nulla quaerat a voluptates voluptate illum voluptatum laborum.</p>
          <img src="./assets/img/store.png" alt="Producto" height="150">
        </div>
        <div class="modal-footer">
          <button class="btn btn-dark" type="button" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>