<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="?c=tendero&m=home">Tienda</a>
    </li>
    <li class="breadcrumb-item active">
      Home
    </li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <a href="?c=tendero&m=validar">
          <div class="p-5 opt">
            <h2 class="textico">Vender</h2>
          </div>
        </a>
        <a href="?c=tendero&m=registrarUsuario">
          <div class="p-5 opt">
            <h2 class="textico">Registrar Usuario</h2>
          </div>
        </a> 
        <a href="?c=tendero&m=registrarProducto">
          <div class="p-5 opt">
            <h2 class="textico">Registrar Producto</h2>
          </div>
          </a> 
      </div>
    </div>
  </div>
</main>