<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="?c=tendero&m=home">Tendero</a>
    </li>
    <li class="breadcrumb-item active">Perfil</li>
  </ol>
  <div class="container">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-6">
          <div class="card text-white bg-warning">
            <div class="card-header">
              <h3>Datos Personales</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Nombres</label>
                <input class="form-control" name="nombres" value="Mona" readonly>
              </div>
              <div class="form-group">
                <label>Apellidos</label>
                <input class="form-control" name="apellidos" value="Chimba" readonly>
              </div>
              <div class="form-group">
                <label>Telefono</label>
                <input class="form-control" name="telefono" value="555 55 55" readonly>
              </div>
              <div class="form-group">
                <label>Celular</label>
                <input class="form-control" name="celular" value="321 456 78 45" readonly>
              </div>
              <div class="form-group">
                <label>Dirección</label>
                <input class="form-control" name="direccion" value="Paris, Francia" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card text-white bg-info">
            <div class="card-header">
              <h3>Información Académica</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Institución</label>
                <input class="form-control" name="institucion" value="Université Paris-Sorbonne" readonly>
              </div>
              <div class="form-group">
                <label>Título</label>
                <input class="form-control" name="titulo" value="Negocios y Finanzas Internacionales" readonly>
              </div>
              <div class="form-group">
                <label>Año de Graduación</label>
                <input class="form-control" name="graduacion" value="1519" readonly>
              </div>
              <div class="form-group">
                <label>Especialización</label>
                <input class="form-control" name="especializacion" value="Arte Renancensita" readonly>
              </div>
              <div class="form-group">
                <label>Otros títulos</label>
                <input class="form-control" name="otrostitulos" value="Ingeniera y Pintura" readonly>
              </div>
            </div>
          </div>
        </div>
      </div>
      <p class="lead">
        <button class="btn btn-outline-warning btn-lg btn-block" tipe="button" data-toggle="modal" data-target="#warningModal">Actualizar Datos</button>
      </p>
    </div>
  </div>
</main>

<div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dark" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Antes de modificar..</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Se ha enviado un correo de confirmación de identidad a <em>mona******@g***.com</em></p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-dark" type="button" data-dismiss="modal">Continuar</button>
      </div>
    </div>
  </div>
</div>