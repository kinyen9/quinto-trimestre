<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="?c=tendero&m=home">Tendero</a>
    </li>
    <li class="breadcrumb-item active">
      Registrar Producto
    </li>
  </ol>
  <div class="container-fluid containercito">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card mx-1">
          <div class="card-body p-5">
            <form class="nv needs-validation" method="POST" enctype="multipart/form-data" novalidate>
              <h3>Ingrese Nuevo Producto</h3>
              <br>
              <label for="validationProduct">Nombre Producto</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupPrepend">⟡</span>
                </div>
                <input type="text" class="form-control" id="validationProduct" placeholder="Chocolate" aria-describedby="inputGroupPrepend" readonly required>
                <div class="invalid-tooltip">
                  Escribe el nombre del producto.
                </div>
              </div>
              <label for="validationDescription">Descripción del Producto</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupPrepend">⟡</span>
                </div>
                <input type="text" class="form-control" id="validationDescription" placeholder="A base de cacao" aria-describedby="inputGroupPrepend" readonly required>
                <div class="invalid-tooltip">
                  Escribe una descripción del producto.
                </div>
              </div>
              <label for="validationWeight">Peso</label>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupPrepend">⟡</span>
                </div>
                <input type="number" class="form-control" id="validationWeight" placeholder="500 Gramos" aria-describedby="inputGroupPrepend" readonly required>
                <div class="invalid-tooltip">
                  Escribe el peso del producto.
                </div>
              </div>
              <div class="input-group mb-3">
                <label for="validationPrice">Precio</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend">$</span>
                  </div>
                  <input type="text" class="form-control" id="validationPrice" placeholder="4500" aria-describedby="inputGroupPrepend" readonly required>
                  <div class="invalid-tooltip">
                    Escribe el precio del producto.
                  </div>
                </div>
              </div>
              <div class="input-group mb-4">
                <label for="validatedCustomFile">Imagen del producto</label>
                <div class="input-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="validatedCustomFile" accept=".jpg, .jpeg, .png" placeholder="Chocolate.png" readonly required>
                    <label class="custom-file-label" for="validatedCustomFile">Chocolate.png</label>
                    <div class="invalid-tooltip">
                      Ingrese una Imagen para el producto
                    </div>
                  </div>
                </div>
              </div>
              <button class="btnito cu" type="button" data-toggle="modal" data-target="#ProductRegistrer">Crear Producto</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<div class="modal fade" id="ProductRegistrer" tabindex="1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-info" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Creado Satisfactoriamente</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">X</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Producto: <b>Chocolate</b></p>
        <p>Descripción: A base de cacao</p>
        <p>Precio: $4500</p>
        <img src="./assets/img/Chocolate.png" alt="Producto" height="200"> 
      </div>
    </div>
  </div>
</div>