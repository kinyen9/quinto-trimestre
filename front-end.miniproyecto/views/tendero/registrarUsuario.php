<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="?c=tendero&m=home">Tendero</a>
        </li>
        <li class="breadcrumb-item active">
            Registrar Usuario
        </li>
    </ol>
    <div class="container-fluid containercito">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mx-1">
                    <div class="card-body p-5">
                        <form class="nv needs-validation" method="POST" novalidate>
                            <h2>Datos del cliente</h2>
                            <br>
                            <label for="validationUsername">Documento</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend">⟡</span>
                                </div>
                                <input type="text" class="form-control" id="validationUsername" placeholder="132456897" aria-describedby="inputGroupPrepend" readonly required>
                                <div class="invalid-feedback">
                                    Escribe el documento del cliente.
                                </div>
                            </div>
                            <label for="validationName">Nombres</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend">⟡</span>
                                </div>
                                <input type="text" class="form-control" id="validationName" placeholder="Pepito" aria-describedby="inputGroupPrepend" readonly required>
                                <div class="invalid-feedback">
                                    Escribe el nombre del cliente.
                                </div>
                            </div>
                            <label for="validationLastName">Apellidos</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend">⟡</span>
                                </div>
                                <input type="text" class="form-control" id="validationLastName" placeholder="Peréz" aria-describedby="inputGroupPrepend" readonly required>
                                <div class="invalid-feedback">
                                    Escribe el apellido del cliente.
                                </div>
                            </div>
                            <div class="input-group mb-4">
                                <label for="validationEmail">Correo Electrónico</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">⟡</span>
                                    </div>
                                    <input type="text" class="form-control" id="validationEmail" placeholder="Pepe@gmail.com" aria-describedby="inputGroupPrepend" readonly required>
                                    <div class="invalid-feedback">
                                        Escribe el correo electrónico del cliente.
                                    </div>
                                </div>
                            </div>
                            <button class="btnito cu" type="button" data-toggle="modal" data-target="#UserRegistrer">Crear Cliente</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="UserRegistrer" tabindex="1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-success" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title miven">Cliente Creado</h3>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
            </div>
        </div>
    </div>
</div>