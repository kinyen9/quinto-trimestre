<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">
      <a href="?c=tendero&m=home">Tienda</a>
    </li>
    <li class="breadcrumb-item">
      Inventario
    </li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <h1>Inventario</h1>
      </div>
      <div class="row">
        <h3>Disponible en tienda</h3>
        <table class="table table-hover">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Cant. Disponible</th>
              <th scope="col">Nombre Producto</th>
              <th scope="col">Descripción del Producto</th>
              <th scope="col">Precio</th>
            </tr>
          </thead>
          <tbody class="table-light">
            <tr>
              <th scope="row">15 Kilo</th>
              <td>Tomate</td>
              <td><a class="preview" rel="./assets/img/Tomate.png" title="">Fruto de la planta conocida como tomatera</a></td>
              <td>$2000 Kilo</td>
            </tr>
            <tr>
              <th scope="row">25 Unid</th>
              <td>Escoba</td>
              <td><a class="preview" rel="./assets/img/Escoba.png" title="">Herramienta para limpiar o barrer el suelo.</a></td>
              <td>$3000 C/U</td>
            </tr>
            <tr>
              <th scope="row">5 Kilos</th>
              <td>Espinaca China</td>
              <td><a class="preview" rel="./assets/img/Espinaca-China.png" title="">Especie perteneciente a la familia Basellaceae.</a></td>
              <td>$6000 Kilo</td>
            </tr>
            <tr>
              <th scope="row">9 Libras</th>
              <td>Granola</td>
              <td><a class="preview" rel="./assets/img/Granola.jpg" title="">Alimento formado por nueces, copos de avena mezclados con miel y otros ingredientes naturales.</a></td>
              <td>$3500 Libra</td>
            </tr>
            <tr>
              <th scope="row">21 Litros</th>
              <td>Leche</td>
              <td><a class="preview" rel="./assets/img/Leche.png" title="">Función es la de nutrir a las crías hasta que sean capaces de digerir otros alimentos.</a></td>
              <td>$2400 Litro</td>
            </tr>
            <tr>
              <th scope="row">1 Kilo</th>
              <td>Chocolate</td>
              <td><a class="preview" rel="./assets/img/Chocolate.png" title="">A base de cacao</a></td>
              <td>$8000 Kilo</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</main>