<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">
      <a href="?c=tendero&m=home">Tienda</a>
    </li>
    <li class="breadcrumb-item">
      Clientes
    </li>
  </ol>
  <div class="container">
    <div class="animated fadeIn">
      <h1>Clientes de tu Tienda</h1>
      <br>
      <br>
      <div class="row">
        <table class="table table-hover">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Número de identificacíon</th>
              <th scope="col">Nombres</th>
              <th scope="col">Apellido</th>
              <th scope="col">Correo Electronico</th>
            </tr>
          </thead>
          <tbody class="table-light">
            <tr>
              <th scope="row">1033741321</th>
              <td>Hernando</td>
              <td>Goméz</td>
              <td>JunitoElRoquerito@gmail.com</td>
            </tr>
            <tr>
              <th scope="row">132456897</th>
              <td>Pepito</td>
              <td>Peréz</td>
              <td>Pepe@gmail.com</td>
            </tr>
            <tr>
              <th scope="row">54123789</th>
              <td>Pedro</td>
              <td>Güimará</td>
              <td>ElPedrazo@hotmail.com</td>
            </tr>
            <tr>
              <th scope="row">4587123654</th>
              <td>Andrea</td>
              <td>Umaña</td>
              <td>Andreita1542@yahoo.com</td>
            </tr>
            <tr>
              <th scope="row">1047854236</th>
              <td>Maria</td>
              <td>Aristizabal</td>
              <td>MariAristi4510@bancolombia.com.co</td>
            </tr>
            <tr>
              <th scope="row">897564</th>
              <td>Juan</td>
              <td>Gonzaléz</td>
              <td>JunitoElRoquerito@gmail.com</td>
            </tr>
          </tbody>
        </table>
        <a href="?c=tendero&m=registrarUsuario">
          <button class="btn btn-info float-">
            <span class="fa fa-plus"></span>Agregar Usuario</button>
        </a>
      </div>
    </div>
  </div>