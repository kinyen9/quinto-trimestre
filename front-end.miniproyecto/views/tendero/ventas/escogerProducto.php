<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">
      <a href="?c=tendero&m=home">Tendero</a>
    </li>
    <li class="breadcrumb-item">
      Vender
    </li>
    <li class="breadcrumb-item">
      Productos
    </li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="card border-warning bg=info">
          <div class="card-header cc">
            <h1><i class="fas fa-thumbtack"></i> Confirmar compra</h1>
          </div>
          <div class="card-body">
            <section class="products">
              <div class="product-card">
                <div class="product-image">
                  <img src="./assets/img/Espinaca-China.png">
                </div>
                <div class="product-info">
                  <h5>Espinaca China</h5>
                  <h6>$6000 | 1 Kilo</h6>
                </div>
              </div>
              <div class="product-card">
                <div class="product-image">
                  <img src="./assets/img/Leche.png">
                </div>
                <div class="product-info">
                  <h5>Leche</h5>
                  <h6>$2400 | 1 Litro</h6>
                </div>
              </div>
              <div class="product-card">
                <div class="product-image">
                  <img src="./assets/img/Chocolate.png">
                </div>
                <div class="product-info">
                  <h5>Chocolate</h5>
                  <h6>$8000 | 1 Kilo</h6>
                </div>
              </div>
              <div class="product-card">
                <div class="product-image">
                  <img src="./assets/img/Granola.jpg">
                </div>
                <div class="product-info">
                  <h5>Granola</h5>
                  <h6>$7000 | 2 Libra</h6>
                </div>
              </div>
              <div class="product-card">
                <div class="product-image">
                  <img src="./assets/img/Tomate.png">
                </div>
                <div class="product-info">
                  <h5>Tomates</h5>
                  <h6>$6000 | 3 Kilos</h6>
                </div>
              </div>
            </section>
          </div>
          <div class="card-footer">
            <button class="btn btn-danger float-left" type="button" data-toggle="modal" data-target="#home">Cancelar</button>
            <button class="btnito cu float-right" type="button" data-toggle="modal" data-target="#modelPreFacture">Confirmar Compra</button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="home" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-danger" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">¿Desea realmente cancelar la compra?</h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">No</button>
          <a href="?c=tendero&m=home">
            <button class="btn btn-danger" type="button">Si, cancelar compra</button>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modelPreFacture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-success" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Exito: <br> <b>¡Compra Realizada!</b></h4>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <h5><b>Confirmación datos del Cliente:</b></h5><br>
          <p>Identicación: <ins>132456897</ins></p>
          <p>Cliente: <b>Pepito Peréz</b></p>
          <p>Correo: <i>Pepe@gmail.com</i></p>
          <p>Metodo de Pago:<b> Efectivo</b></p>
          <p></p>
          <img src="./assets/img/GraciasCompra.jpg" alt="Gracias" height="100">
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
          <a href="?c=tendero&m=home">
            <button class="btn btn-outline-dark" type="button"><i class="fab fa-amazon-pay"></i> Salir sin Factura</button>
          </a>
          <a href="?c=tendero&m=facturar">
            <button class="btn btn-outline-dark" type="button"><i class="fas fa-receipt"></i> Generar Factura</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</main>