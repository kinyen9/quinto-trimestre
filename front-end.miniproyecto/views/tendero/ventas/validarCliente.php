<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">
      <a href="?c=tendero&m=home">Tendero</a>
    </li>
    <li class="breadcrumb-item">
      Vender
    </li>
    <li class="breadcrumb-item">
      Validar Cliente
    </li>
  </ol>
  <div class="container-fluid cl">
    <div class="animated fadeIn">
      <div class="row justify-content-center">
        <div class="card p-5">
          <div class="card-body">
            <form action="" class="nv needs-validation" method="post" novalidate>
              <h3 for="docvalido">Documento del Cliente</h3>
              <br>
              <div class="form-group">
                <div class="input-group mb-4">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <i class="fa fa-asterisk"></i>
                    </span>
                  </div>
                  <input class="form-control" id="docvalido" type="number" name="docvalido" autocomplete="Documento Cliente" placeholder="132456897" aria-describedby="inputGroupPrepend" readonly required>
                  <div class="invalid-feedback">
                    Escribe el documento del usuario.
                  </div>
                </div>
              </div>
              <div class="form-group form-actions">
                <button class="btnito cu" type="button" data-toggle="modal" data-target="#successModal">Validar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-info" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Bienvenido: <b>Mona Chimba</b></h4>
          <div class="w-100"></div>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Identicación: <ins>132456897</ins></p>
          <p>Cliente: <b>Pepito Peréz</b></p>
          <p>Correo: <i>Pepe@gmail.com</i></p>
          <div class="form-group">
            <label for="mp">Selecciona un metodo de pago</label>
            <select id="mp" class="custom-select custom-select-sm" multiple required>
              <option select value="1">Efectivo <b>[Predeterminado]</b></option>
              <option value="2">Consignación Bancaria</option>
              <option value="3">Tarjeta de Credito</option>
            </select>
          </div>
          <p></p>
          <img src="./assets/img/Cliente.png" alt="Cliente" height="200">
        </div>
        <div class="modal-footer">
          <a href="?c=tendero&m=home">
            <button class="btn btn-secondary" type="button">Cancelar</button>
          </a>
          <a href="?c=tendero&m=escoger">
            <button class="btn btn-info" type="button">Continuar</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</main>