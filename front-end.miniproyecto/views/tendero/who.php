<main class="main">
    <!-- Breadcrumb-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="?c=tendero&m=home">Tienda</a>
        </li>
        <li class="breadcrumb-item active">
            Quienes Somos
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col">
                    <h3><b>Nuestra Tienda</b></h3>
                    <br>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed urna quis nisl facilisis sollicitudin. Curabitur et mi erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla ut massa eu magna viverra iaculis. Pellentesque tristique mi diam, id commodo metus dictum id. Mauris sagittis mattis mi, a consequat nunc efficitur a. Curabitur at nulla ac sapien maximus euismod dapibus et ante. Mauris venenatis euismod libero in iaculis. Quisque in nisi congue, tincidunt diam quis, posuere magna. Vivamus felis velit, vestibulum a viverra eget, accumsan vel massa. Sed eget luctus magna. Integer consequat aliquet augue sit amet sodales. </p>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <p>Etiam vestibulum velit at ante suscipit, ac sollicitudin metus laoreet. Cras iaculis eros sed urna vestibulum placerat. Suspendisse sit amet tristique metus. Vestibulum pharetra tortor sit amet laoreet commodo. Nunc eu nisl sit amet nunc fringilla luctus at nec diam. Cras finibus turpis eget tincidunt semper. Fusce aliquam erat nibh. Maecenas mollis dictum enim. Aliquam lacinia justo dui, et euismod orci aliquet ac. Nulla viverra tortor at mauris tristique, et dignissim leo tristique. Donec efficitur dolor augue, ut porttitor dui pharetra sed. </p>
                </div>
                <div class="w-100">
                    <p> Nam id erat velit. Pellentesque placerat sodales eros, a pellentesque velit blandit eu. Duis non sapien et justo fermentum sodales id vel neque. Sed velit magna, porttitor in rhoncus ut, sagittis vel magna. Pellentesque dui sapien, tincidunt eu congue nec, commodo nec tellus. Morbi mollis eros eget velit facilisis, non ullamcorper libero ultricies. Nullam sed tempor eros, sit amet consectetur magna. In placerat, odio a convallis tempus, dolor risus rutrum quam, ut condimentum dolor dolor in ante. Nam tristique tristique hendrerit. Curabitur nec sapien ac nisi volutpat facilisis. Donec sed neque sit amet nunc ultrices pulvinar et ultricies nisl. Aliquam erat volutpat. </p>
                </div>
                <div class="w-100">
                    <p> Maecenas non nisi faucibus, consequat risus eget, sollicitudin dolor. Duis fermentum augue ut metus feugiat, nec ultrices elit consequat. Curabitur pulvinar posuere metus, at tincidunt nisl semper in. In eget magna a libero luctus imperdiet. Suspendisse eget ante id dolor blandit sagittis. Aliquam pellentesque lorem sapien, sed vestibulum sapien gravida ut. Nunc viverra dui sem, quis commodo arcu semper quis. Suspendisse maximus velit sed tellus accumsan, sit amet vestibulum mi sollicitudin. Ut semper velit at malesuada ornare. Donec nec pellentesque sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p>
                </div>
                <div class="w-100">
                    <p> Sed tincidunt nunc sit amet urna molestie feugiat. Pellentesque mattis interdum placerat. Sed sit amet sapien interdum ligula finibus finibus a eget turpis. Nullam feugiat bibendum pretium. Cras at neque aliquam ante consequat imperdiet in nec augue. Aenean eu urna neque. Aenean interdum nisi neque, eu semper elit posuere eu. Praesent libero nibh, elementum sit amet lorem vel, laoreet lobortis ligula. Duis id dui id dui faucibus luctus. Morbi ultrices tincidunt felis eu fermentum. </p>
                </div>
                <div class="w-100">
                    <h2><b>Gracias por Visitarnos!</b></h2>
                </div>
            </div>
        </div>
</main>