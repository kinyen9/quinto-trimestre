<?php

class UsuariosController{
    
    private $modelUsuario;
    
    public function __construct(){
        try{
           $this->modelUsuario = new Usuario();
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }
    
    public function home() {
        require_once 'views/all/header.php';
        require_once 'views/home.php';
        require_once 'views/all/footer.php';
    }

    public function registrarInformacion(){
        $destino='assets/FILES/IMG';
        $tmp_name = $_FILES["url_imagen"]["tmp_name"];
        $name = basename($_FILES["url_imagen"]["name"]);
        $destinofinal=$destino.'/'.$name;
        move_uploaded_file($tmp_name, $destino.'/'.$name);
        $nombres = $_POST['nombres'];
        $apellidos = $_POST['apellidos'];
        $descripcion = $_POST['descripcion'];
        $url_imagen = $destinofinal;
        $this->modelUsuario->crear($nombres, $apellidos, $descripcion, $url_imagen);
        header("location:?c=Usuarios&m=home");
    }
    
    public function eliminarInformacion(){
        unlink($_POST['url_imagen']);
        $this->modelUsuario->eliminar($_POST['idusuario']);
        header("location:?c=Usuarios&m=home");
    }
    public function modUsuario(){
        require_once 'views/all/header.php';
        require_once 'views/usuario/mod.php';
        require_once 'views/all/footer.php';
    }

    public function modInformacion(){
        $idusuario = $_POST["idusuario"];
        $nombres = $_POST['nombres'];
        $apellidos = $_POST['apellidos'];
        $descripcion = $_POST['descripcion'];
        if (!isset($_POST['foto'])) {
            $url_imagen = $_POST['url_imagen'];
        } elseif (isset($_POST['foto'])) {
            $destino='assets/FILES/IMG';
            $tmp_name = $_FILES["foto"]["tmp_name"];
            $name = basename($_FILES["foto"]["name"]);
            $destinofinal=$destino.'/'.$name;
            move_uploaded_file($tmp_name, $destino.'/'.$name);
            if (isset($_POST['url_imagen'])) {
                unlink($_POST['url_imagen']);
            }
            $url_imagen = $destinofinal;
        }
        $this->modelUsuario->editar($idusuario,$nombres,$apellidos,$descripcion,$url_imagen);
        header("location:?c=Usuarios&m=home");
    }
}