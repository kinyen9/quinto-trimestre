<?php

Class Database {
    
    public static function Conectar(){
        try{
            $pdo= new PDO('mysql:host=localhost;dbname=db_img_server;charset=utf8','root','');
            $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                return $pdo;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}

?>