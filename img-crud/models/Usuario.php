<?php

class Usuario{
    
    private $pdo;
    
    Public function __construct(){
        try{
        $this->pdo=Database::Conectar();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }    

    public function crear($nombres,$apellidos,$descripcion,$url_imagen){
        try{
            $stm=$this->pdo->prepare("INSERT INTO Usuario(nombres,apellidos,descripcion,url_imagen)VALUES(?,?,?,?)");
            $stm->bindParam(1,$nombres,PDO::PARAM_STR);
            $stm->bindParam(2,$apellidos,PDO::PARAM_STR);
            $stm->bindParam(3,$descripcion,PDO::PARAM_STR);
            $stm->bindParam(4,$url_imagen,PDO::PARAM_STR);
            $stm->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }    
    
    public function consultar(){
        try{
            $stm=$this->pdo->prepare("SELECT * FROM Usuario");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function editar($idusuario,$nombres,$apellidos,$descripcion,$url_imagen){
        try {
			$stm=$this->pdo->prepare("UPDATE Usuario SET nombres=?,apellidos=?,descripcion=?,url_imagen=? WHERE idusuario = ?");
            $stm->bindParam(1,$idusuario,PDO::PARAM_INT);
            $stm->bindParam(2,$nombres,PDO::PARAM_STR);
            $stm->bindParam(3,$apellidos,PDO::PARAM_STR);
            $stm->bindParam(4,$descripcion,PDO::PARAM_STR);
            $stm->bindParam(5,$url_imagen,PDO::PARAM_STR);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
    }
    
    public function eliminar($idusuario){
        try {
            $stm=$this->pdo->prepare("DELETE FROM usuario WHERE idusuario = ?; ALTER TABLE Usuario AUTO_INCREMENT = 1;");
            $stm->bindParam(1,$idusuario,PDO::PARAM_INT);
            $stm->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        } 
    }
}