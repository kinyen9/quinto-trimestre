<br>
<div class="container"> 
  <div class="row"> 
    <div class="col-md-8">
        <table class="table">
            <tr>
                <th>Foto</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Descripcion</th>
                <th>Modificar</th>
                <th>Eliminar</th>
            </tr>
        <?php
        foreach ($this->modelUsuario->consultar() as $value){
         ?>
            <tr><td><img width=50px src="<?php echo $value->url_imagen; ?>" /></td>
                <td><?php echo $value->nombres; ?></td>
                <td><?php echo $value->apellidos; ?></td>
                <td><?php echo $value->descripcion; ?></td>
                <td><form action="?c=Usuarios&m=modUsuario" method="POST">
                    <input type="hidden" name="idusuario" id="idusuario" value="<?php echo $value->idusuario ?>">
                    <input type="hidden" name="url_imagen" id="url_imagen" value="<?php echo $value->url_imagen ?>">
                <button class="btn btn-warning btn-sm">Modificar</button></form></td>
                <td><form action="?c=Usuarios&m=eliminarInformacion" method="POST">
                    <input type="hidden" name="idusuario" id="idusuario" value="<?php echo $value->idusuario ?>">
                    <input type="hidden" name="url_imagen" id="url_imagen" value="<?php echo $value->url_imagen ?>">
                    <button class="btn btn-danger btn-sm">Eliminar</button></form></td>
            </tr>
        <?php
        }
         ?>
       </table> 
    </div>    
          <div class="col-md-4">
              <form action="?c=Usuarios&m=registrarInformacion" method="POST" enctype="multipart/form-data">
            <div class="form-group">  
                <label for="nombres">Nombres:</label>
                <input class="form-control" type="text" name="nombres" id="nombres">
            </div>
            <div class="form-group">  
                <label for="apellidos">Apellidos:</label>
                <input class="form-control" type="text" name="apellidos" id="apellidos">
            </div>
            <div class="form-group">  
                <label for="descripcion">Descripción:</label>
                <textarea class="form-control" type="text" name="descripcion" id="descripción"></textarea>
            </div>
            <div class="form-group">  
                <label for="url_imagen">Agregue su Imagen:</label>
                <input type="file" name="url_imagen" id="url_imagen">
            </div>
            <hr>
            <button class="btn btn-info">Registrar</button>
          </form>
        </div>
    </div>
 </div>
