<?php 
foreach ($this->modelUsuario->consultar($_POST['idusuario']) as $value) {
}
 ?>
 <div class="container">
    <br><h2>Modificar Usuarios</h2><hr>
    <div class="row">
        <div class="col-md-4">
            <form action="?c=Usuarios&m=modInformacion" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="nombres">Nombres</label>
                    <input type="text" name="nombres" id="nombres" class="form-control" value="<?php echo $value->nombres ?>">
                </div>
                <div class="form-group">
                    <label for="apellidos">Apellidos</label>
                    <input type="text" name="apellidos" id="apellidos" class="form-control" value="<?php echo $value->apellidos ?>">
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripción</label>
                    <input type="text" name="descripcion" id="descripcion" class="form-control" value="<?php echo $value->descripcion ?>">
                </div>
                <div class="form-group">
                    <label for="url_imagen">Foto Actual</label>
                </div>
                <div class="form-group">
                    <img width=200px src="<?php echo $value->url_imagen; ?>" />
                </div>
                <div class="form-group">
                    <label for="foto">Ingrese Nueva Foto</label>
                </div>
                <div class="form-group">
                    <input type="file" name="foto" id="foto">
                </div>
                <hr>
                <input type="hidden" name="idusuario" id="idusuario" value="<?php echo $value->idusuario ?>">
                <input type="" name="url_imagen" id="url_imagen" value="<?php echo $_POST['url_imagen'] ?>">
                <input type="" name="foto" id="foto" value="<?php echo $_POST['foto'] ?>">
                <button class="btn btn-info">Modificar</button>
            </form>
        </div>
    </div>
 </div>