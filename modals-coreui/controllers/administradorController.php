<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of administradorController
 *
 * @author Henderson Moreno
 */
class administradorController {
    
    public function dashboard(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/administrador/dashboard.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }
    
    public function perfil(){
        require_once 'views/all/header.php';
        require_once 'views/all/navbar.php';
        require_once 'views/all/sidebar.php';
        require_once 'views/administrador/perfil.php';
        require_once 'views/all/aside.php';
        require_once 'views/all/footer.php';
    }
}