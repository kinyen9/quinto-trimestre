<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="?c=administrador&m=Dashboard">Dashboard</a></li>
    <li class="breadcrumb-item">
      Gestion de usuarios
    </li>
    <!-- Breadcrumb Menu-->
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <br>
      
      <div class="card">
        <div class="card-body">
          <button class="btn btn-info" data-toggle="modal" data-target="#modal_registrar_usuario"><span class="fa fa-plus"></span>Agregar Usuario</button>
          <button class="btn btn-info" data-toggle="modal" data-target="#modal_busqueda_avanzada"><span class="fa fa-search"></span>Busqueda Avanzada</button>
          <br><br>
          <table class="table table-bordered table-sm">
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Correo</th>

              <th>Telefono</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
            <tr>
              <td>0001</td>
              <td>Robin</td>
              <td>Cortes</td>
              <td>Un@jeje.com</td>
              <td>56565</td>
              <td><a class="btn btn-info btn-sm" href=""><span class="fa fa-edit"> Editar</span></a></td>
              <td><a class="btn btn-danger btn-sm" href=""><span class="fa fa-trash-o"> Eliminar</span></a></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</main>

<!-- The Modal -->
<div class="modal" id="modal_registrar_usuario">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Formulario de Registro Usuario</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <form action="action">
              <div class="form-gruop">
                  <label>Nombre</label>
                  <input class="form-control" name="nombre" id="nombre">
              </div>
              <div class="form-gruop">
                  <label>Apellido</label>
                  <input class="form-control" name="apellido" id="apellido">
              </div>
              <div class="form-gruop">
                  <label>Correo</label>
                  <input class="form-control" name="correo" id="correo">
              </div>
              <div class="form-gruop">
                  <label>Telefono</label>
                  <input class="form-control" name="telefono" id="telefono">
              </div>
              <hr>
              <button class="btn btn-success">Registrar Usuario</button>   
          </form>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-toolbar btn-sm" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal" id="modal_busqueda_avanzada">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Cabezera</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-toolbar btn-sm" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>