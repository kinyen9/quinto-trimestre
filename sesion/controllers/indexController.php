<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author Henderson Moreno
 */

class IndexController {
    // Variable privada que contiene todas las funciones de la clase Usuario() y Security();
    private $modelUsuario;
    private $modelSecurity;

    public function __construct(){
        try {
            $this->modelUsuario = new Usuario();
            $this->modelSecurity = new Security();
        } catch (Exception $e){
            die ($e->getMessage());
        }
    }

    public function index() {
        $titulo = '';
        require_once 'views/all/header.php';
        require_once 'views/index/index.php';
        require_once 'views/all/header.php';
    }

    //Formulario registro de un nuevo usuario
    public function register(){
    	$titulo = '';
        require_once 'views/all/header.php';
        require_once 'views/index/register.php';
        require_once 'views/all/header.php';
    }

    // Formulario para iniciar sesión
    public function login(){
    	$titulo = '';
        require_once 'views/all/header.php';
        require_once 'views/index/login.php';
        require_once 'views/all/header.php';
    }

    public function store_user(){
        $data = array($_POST['names'],$_POST['email'],$_POST['password']);
        // var_dump = imprime el contenido de un arrat o vector
        // var_dump($data);
        $this->modelUsuario->create($data);
    }
    
    public function auth_user(){
        //$data = array($_POST['email'],$_POST['password']);
        //var_dump($data);
        
        // Datos que ingreso el usuario por medio
        $correo_form=$_POST['email'];
        $password_form=$_POST['password'];
        foreach ($this->modelUsuario->consult_email($_POST['email']) as $value){}
        //var_dump($value);
        // Si el correo que ingreso el usuario = al correo que esta en la base de datos
        // Si la contraseña que ingreso el usuario = a la contraseña que esta en la base de datos
        
        // y  = and ,&&
        // o = or, ||
        if(($correo_form==$value->correo) && ($password_form==$value->password)){
            // Generar la instancia del metodo de security Auth
            $this->modelSecurity->Auth($correo_form);
            // Redireciona a la vista del administrador
            header("location:?c=administrador&m=home");
        }else{
            // Redireciona al formulario 
            header("location:?c=index&m=login");
        }
    }
    
    public function destroy_user(){
        $this->modelSecurity->Destroy();
        header('location:?c=index&m=login');
    }
}
	
?>