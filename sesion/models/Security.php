<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Security
 *
 * @author Henderson Moreno
 */

class Security extends DB{
    // Esta función solo se iniciará si las credenciales son correctas
    // el proposito de está función es inicializar los datos del usuario en un sesion $_SESSION[]
    public function Auth($correo){
        try {
            $stm = parent::Conectar()->prepare("SELECT * FROM usuarios WHERE correo = ?");
            $stm->bindParam(1,$correo,PDO::PARAM_STR);
            $stm->execute();
            // Se inicializan los datos de la persona en la variable $_SESSION['USER']
            // $_SESSION['USER'] = queda convertido en una array de datos
            $_SESSION['USER']=$stm->fetch(PDO::FETCH_OBJ);
            // Diferencia entre fetch y fetchAll ¿Investigar!?
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }
    // Desinicializa las variables de sesion creadas en el sistema
    // Y despues las destruye
    public function Destroy(){
        try {
            // unset = eliminar el valor de una variable y la deja vacia.
            unset($_SESSION['USER']);
            // destruye todas las variables $_SESSION[]
            session_destroy();
        } catch (Exception $e) {
            die ($e->getMessage());
        }
    }
    // Valida si la sesion de un usuario esta activa y de no ser asi lo sacara 
    // de inmediato de la pagina donde no tiene permisos
    public function ValidateSession(){
        // O !isset funciona en este al igual que empty
        if(empty($_SESSION['USER']) OR is_null($_SESSION['USER'])){
            header ("location:?c=index&m=login");
        }
    }
}
