<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Henderson Moreno
 */

// extends = Herencia de la clase DB
class Usuario extends DB{
    
    // Crear un nuevo usuario
    public function create($data){
        try {
            $stm=parent::Conectar()->prepare("INSERT INTO usuarios (nombres,correo,password) VALUES (?,?,?)");
            // bindParam = uno por cada (?) que se encuentre en la query
            $stm->bindParam(1,$data['0'],PDO::PARAM_STR);
            $stm->bindParam(2,$data['1'],PDO::PARAM_STR);
            $stm->bindParam(3,$data['2'],PDO::PARAM_STR);
            $stm -> execute();            
        }  catch (Exception $e){
            die ($e->getMessage());
        }
    }
    
    // Consultar todos los usuarios registrados
    public function consult(){
        try {
            
        }  catch (Exception $e){
            die ($e->getMessage());
        }
    }
    
    // Funcion para consultar por email/correo
    public function consult_email($correo){
        try {
            $stm=parent::Conectar()->prepare("SELECT * FROM usuarios WHERE correo = ?");
            $stm->bindParam(1,$correo,PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        }  catch (Exception $e){
            die ($e->getMessage());
        }
    }
}
