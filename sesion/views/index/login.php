<div class="container">
	<div class="row">
		<div class="col-md-4">
			<!-- enctype="multipart/form-data" --- soporta $_FILES[]; -->
			<form action="?c=index&m=auth_user" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="">Correo Electronico:</label>
					<input class="form-control" type="email" name="email" id="email" required="">
				</div>
				<div class="form-group">
					<label for="">Contraseña:</label>
					<input class="form-control" type="password" name="password" id="password" required="">
				</div>
				<button class="btn btn-primary" type="submit">Autenticarme</button>
			</form>
		</div>
	</div>
</div>