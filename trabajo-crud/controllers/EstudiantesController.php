<?php 

class EstudiantesController{

	private $modelEstudiantes;

	public function __construct(){
		try {
			$this ->modelEstudiantes = new Estudiantes();
		} catch (Exception $e) {
			
		}
	}
		public function dashboard(){
			require_once('views/all/header.php');
			require_once('views/all/navbar.php');
			require_once('views/dashboard.php');
			require_once('views/all/footer.php');
		}

		public function crearEstudiantes(){
			$tipoDocumento = $_POST['tipoDocumento'];
			$documento = $_POST['documento'];
			$nombres = $_POST['nombres'];
			$apellidos = $_POST['apellidos'];
			$correo = $_POST['correo'];
			$edad = $_POST['edad'];
			$genero = $_POST['genero'];
			$rh = $_POST['rh'];
			$ficha = $_POST['ficha'];
			$this->modelEstudiantes->crear($tipoDocumento,$documento,$nombres,$apellidos,$correo,$edad,$genero,$rh,$ficha);
			header("location:?c=Estudiantes&m=estudiantesConsulta");
		}

		public function eliminarEstudiante(){
			$consAprendiz=$_POST['consAprendiz'];
			$this->modelEstudiante->eliminarEstudiante($consAprendiz);
			header("location:?c=Estudiantes&m=estudiantesConsulta");
		}

		public function estudiantesConsulta(){
			require_once('views/all/header.php');
			require_once('views/all/navbar.php');
			require_once('views/Estudiantes/estudiantesCeet.php');
			require_once('views/all/footer.php');
		}

		public function modificarEstudiante() {
			require_once('views/all/header.php');
			require_once('views/Estudiantes/modificarAprendices.php');
			require_once('views/all/footer.php');
		}

		public function modificarAprendices(){
			$tipoDocumento=$_POST['tipoDocumento'];
			$documento=$_POST['documento'];
			$nombres=$_POST['nombres'];
			$apellidos=$_POST['apellidos'];
			$correo=$_POST['correo'];
			$edad=$_POST['edad'];
			$genero=$_POST['genero'];
			$rh=$_POST['rh'];
			$ficha=$_POST['ficha'];
			$consAprendiz=$_POST['consAprendiz'];
			$this->modelEstudiante->actualizarEstudiantes($tipoDocumento,$documento,$nombres,$apellidos,$correo,$edad,$genero,$rh,$ficha,$consAprendiz);
			header("location:?c=Estudiantes&m=estudiantesConsulta");
		}

		public function consultarTipDocument(){
			try {
				$stm=$this->pdo->prepare("SELECT tipoDoc FROM tipodocumento");
				$stm->execute();
				return $stm->fetchAll();
			} catch (exception $e) {
				die($e->getMessage());
			}
		}
	}
 ?>