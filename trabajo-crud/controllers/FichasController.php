<?php 

class FichasController{
	
	private $modelFichas;

	public function __construct(){
		try {
			$this->modelFichas= new Fichas();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	public function dashboard(){
		require_once('views/all/header.php');
		require_once('views/all/navbar.php');
		require_once('views/Fichas/formFichas.php');
		require_once('views/all/footer.php');
	}

	public function crearFichas(){
		$programaFormacion=$_POST['programaFormacion'];
		
		$this->modelFichas->crearFichas($programaFormacion);
		header("location:?c=Fichas&m=dashboard");
	}

	public function eliminarFichas(){
		$fichaAprendizaje=$_POST['fichaAprendizaje'];
		$this->modelFichas->eliminarFichas($fichaAprendizaje);
		header("location:?c=Fichas&m=dashboard");
	}

	public function modificarFicha() { 
		require_once('views/all/header.php'); 
		require_once('views/Fichas/modificarFichas.php');
		require_once('views/all/footer.php');
	}

	public function modificarFichas(){
		$programaFormacion=$_POST['programaFormacion'];
		$fichaAprendizaje=$_POST['fichaAprendizaje'];
		$this->modelFichas->actualizarFichas($programaFormacion,$fichaAprendizaje);
		header("location:?c=Fichas&m=dashboard");	
	}
}
 ?>