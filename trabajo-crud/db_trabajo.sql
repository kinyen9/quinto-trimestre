create database SENACEET;
use SENACEET;


create table tipoDocumento (
cons int auto_increment,
tipoDoc varchar (30) not null,
primary key (cons)
);

INSERT INTO tipoDocumento (tipoDoc) VALUES
('TI'),
('CC'),
('CE');

create table Fichas (
fichaAprendizaje  int auto_increment,
programaFormacion varchar (30) not null,
primary key (fichaAprendizaje)
);

INSERT INTO Fichas (programaFormacion) VALUES
('ADSI'),
('TPS');

create table Aprendices(
consAprendiz int auto_increment,
tipoDocomento int,
documento int (12) not null,
nombres varchar (40) not null,
apellidos varchar (40) not null,
correo varchar (40) not null,
edad int (2),
genero varchar (6),
rh varchar(6),
ficha int,
primary key (consAprendiz,ficha),
constraint Fk_ficha_ficha foreign key (ficha) references Fichas (fichaAprendizaje),
foreign key (tipoDocomento) references tipoDocumento (tipoDoc)
);



describe aprendices;

SELECT tipodocumento.tipoDoc,documento,nombres,apellidos,correo,edad,genero,rh,fichas.programaFormacion
 FROM aprendices 
 INNER JOIN tipodocumento ON aprendices.tipoDocomento = tipodocumento.cons 
 INNER JOIN fichas ON aprendices.ficha = fichas.fichaAprendizaje;