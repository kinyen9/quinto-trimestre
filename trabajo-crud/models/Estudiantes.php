<?php 

class Estudiantes{

	private $pdo;

	public function __construct(){
		try {
			$this->pdo = Database::Conectar();
		} catch (Exception $e) {
			die ($e->getMessage());
		}
	}

	public function crear($tipoDocumento,$documento,$nombres,$apellidos,$correo,$edad,$genero,$rh,$ficha){
		try {
			$stm=$this->pdo->prepare("INSERT INTO Estudiantes(tipoDocumento,documento,nombres,apellidos,correo,edad,genero,rh,ficha) VALUES (?,?,?,?,?,?,?,?,?)");
			$stm->bindParam(1,$tipoDocumento,PDO::PARAM_INT);
			$stm->bindParam(2,$documento,PDO::PARAM_INT);
			$stm->bindParam(3,$nombres,PDO::PARAM_STR);
			$stm->bindParam(4,$apellidos,PDO::PARAM_STR);
			$stm->bindParam(5,$correo,PDO::PARAM_STR);
			$stm->bindParam(6,$edad,PDO::PARAM_INT);
			$stm->bindParam(7,$genero,PDO::PARAM_STR);
			$stm->bindParam(8,$rh,PDO::PARAM_STR);
			$stm->bindParam(9,$ficha,PDO::PARAM_INT);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function consultarEstudiantes(){
		try {
			$stm=$this->pdo->prepare("SELECT * FROM Estudiantes INNER JOIN tipoDocument INNER JOIN Fichas WHERE Estudiantes.ficha = Fichas.fichaAprendizaje");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die ($e->getMessage());
		}
	}

	public function actualizarEstudiantes($tipoDocumento,$documento,$nombres,$apellidos,$correo,$edad,$genero,$rh,$ficha,$consAprendiz){
		try {
			$stm=$this->pdo->prepare("UPDATE Estudiantes SET tipoDocumento = ?,documento = ?,nombres = ?,apellidos = ?,correo = ?,edad = ?,genero = ?,RH = ?,Ficha=? WHERE consAprendiz = ?");
			$stm->bindParam(1,$tipoDocumento,PDO::PARAM_INT);
			$stm->bindParam(2,$documento,PDO::PARAM_INT);
			$stm->bindParam(3,$nombres,PDO::PARAM_STR);
			$stm->bindParam(4,$apellidos,PDO::PARAM_STR);
			$stm->bindParam(5,$correo,PDO::PARAM_STR);
			$stm->bindParam(6,$edad,PDO::PARAM_INT);
			$stm->bindParam(7,$genero,PDO::PARAM_STR);
			$stm->bindParam(8,$rh,PDO::PARAM_STR);
			$stm->bindParam(9,$ficha,PDO::PARAM_INT);
			$stm->bindParam(10,$consAprendiz,PDO::PARAM_INT);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function eliminarEstudiante($consAprendiz){
		try {
			$stm=$this->pdo->prepare("DELETE FROM estudiantes WHERE consAprendiz = ?");
			$stm->bindParam(1,$consAprendiz,PDO::PARAM_INT);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function consultar_tip(){
		try {
			$stm=$this->pdo->prepare("SELECT tipoDoc FROM tipodocumento");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

}

 ?>