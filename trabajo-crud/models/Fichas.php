<?php 

class Fichas{
	
	private $pdo;

	public function __construct(){
		try {
			$this->pdo = Database::Conectar();
		} catch (Exception $e) {
			die ($e->getMessage());
		}
	}

	public function crearFichas($programaFormacion){
		try {
			$stm=$this->pdo->prepare("INSERT INTO Fichas(programaFormacion) VALUES (?)");
			$stm->bindParam(1,$programaFormacion,PDO::PARAM_STR);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function consultarFichas(){
		try {
			$stm=$this->pdo->prepare("SELECT * FROM Fichas");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die ($e->getMessage());
		}
	}

	public function eliminarFichas($fichaAprendizaje){
		try {
			$stm=$this->pdo->prepare("DELETE FROM Fichas WHERE fichaAprendizaje = ?");
			$stm->bindParam(1,$fichaAprendizaje,PDO::PARAM_INT);
			$stm->execute();
		} catch (Exception $e) {
			die ($e->getMessage());
		}
	}

	public function actualizarFichas($programaFormacion,$fichaAprendizaje){
		try {
			$stm=$this->pdo->prepare("UPDATE Fichas SET programaFormacion = ? WHERE fichaAprendizaje = ?");
			$stm->bindParam(1,$programaFormacion,PDO::PARAM_STR);
			$stm->bindParam(2,$fichaAprendizaje,PDO::PARAM_INT);
			$stm->execute();
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
}
