<?php
  $mysqli = new mysqli('localhost', 'root', '', 'senaceet');
?>
<?php
$doc_est = $_POST['doc_est'];
foreach ($this -> modelEstudiante -> consultar_doc($doc_est) as $valueEst) {}
?>
<div class="container">
	<div class="row">
	<h2>Modificar Aprendices</h2>
	<br><hr>
	<p>No se deben dejar espacios vacios en el formulario</p>
		<div class="col-md-8">
			<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<tr>
					<th>Tipo de documento</th>
					<th>Documento</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Correo</th>
					<th>Edad</th>
					<th>Genero</th>
					<th>RH</th>
					<th>Ficha</th>
				</tr>
				<?php 
				foreach ($this->modelEstudiante->consultarEstudiante() as $value ) {
					if ($value->consAprendiz == $consAprendiz=$_POST['consAprendiz']){
					?>
				<tr>
					<td><?php echo $value->tipoDoc ?></td>
					<td><?php echo $value->documento ?></td>
					<td><?php echo $value->nombres ?></td>
					<td><?php echo $value->apellidos ?></td>
					<td><?php echo $value->correo ?></td>
					<td><?php echo $value->edad ?></td>
					<td><?php echo $value->genero ?></td>
					<td><?php echo $value->rh ?></td>
					<td><?php echo $value->programaFormacion ?></td>
				</tr>
			</table>
		</div>
		</div>
		<div class="col-md-4">
			<form action="?c=Estudiantes&m=modificarAprendices" method="post">
				<div class="form-group">
					<label for="Tipodocumento">Tipo de documento</label>
					<select class="form-control" name="tipoDocomento" id="tipoDocomento">
						<option value="0">SELECCIONAR...</option>
					<?php
					$query = $mysqli -> query ("SELECT * FROM tipodocumento");
					while ($valores = mysqli_fetch_array($query)) {
						echo '<option value="'.$valores[cons].'">'.$valores[tipoDoc].'</option>';
					}
					?>
					</select>
				</div>

				<div class="form-group">
					<label for="documento">Documento</label>
					<input type="text" name="documento" id="documento" class="form-control" value="<?php echo $value->documento ?>">
				</div>

				<div class="form-group">
					<label for="nombres">Nombres</label>
					<input type="text" name="nombres" id="nombres" class="form-control" value="<?php echo $value->nombres ?>">
				</div>
						
				<div class="form-group">
					<label for="apellidos">Apellidos</label>
					<input type="text" name="apellidos" id="apellidos" class="form-control" value="<?php echo $value->apellidos ?>">
				</div>
						
				<div class="form-group">
					<label for="correo">Correo</label>
					<input type="text" name="correo" id="correo" class="form-control" value="<?php echo $value->correo ?>">
				</div>
						
				<div class="form-group">
					<label for="edad">Edad</label>
					<input type="text" name="edad" id="edad" class="form-control" value="<?php echo $value->edad ?>">
				</div>
				<div class="form-group">
					<label for="genero">Genero</label>
					<select class="form-control" name="genero" id="genero">
						<option value="hombre">Masculino</option>
						<option value="mujer">Femenino</option>
					</select>
				</div>
				<div class="form-group">
					<label for="rh">Rh</label>
					<select class="form-control" name="RH" id="RH">
						<option value="O +">O Positivo (+)</option>
						<option value="O -">O Negativo (-)</option>
						<option value="A +">A Positivo (+)</option>
						<option value="A -">A Negativo (-)</option>
						<option value="B +">B Positivo (+)</option>
						<option value="B -">B Negativo (-)</option>
						<option value="AB +">AB Positivo (+)</option>
						<option value="AB -">AB Negativo (-)</option>
					</select>
				</div>
				<div class="form-group">
					<label for="ficha">Ficha</label>
					<select class="form-control" name="ficha" id="ficha">
						<option value="0">Seleccione:</option>
						<?php
						$query = $mysqli -> query ("SELECT * FROM fichas");
						while ($valores = mysqli_fetch_array($query)) {
							echo '<option value="'.$valores[fichaAprendizaje].'">'.$valores[programaFormacion].'</option>';
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<label for="consAprendiz"></label>
					<input type="hidden" name="consAprendiz" id="consAprendiz" class="form-control" value="<?php echo $value->consAprendiz ?>">
				</div>
				<hr>
				<center><button class="btn btn-success">Actualizar</button></center>
				<?php
						}
					}
					?>
			</form>
		</div>
	</div>
</div>