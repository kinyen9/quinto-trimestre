<div class="container">
	<div class="row">
		<h2>Aprendices matriculados</h2>
		<hr>
		<div class="col-md-12">
			<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<tr>
					<th>Tipo de documento</th>
					<th>Documento</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Correo</th>
					<th>Edad</th>
					<th>Genero</th>
					<th>RH</th>
					<th>Ficha</th>
				</tr>
				<?php 
				foreach ($this->modelEstudiantes->consultarEstudiantes() as $value) {
					?>
				<tr>
					<td><?php echo $value->tipoDoc ?></td>
					<td><?php echo $value->documento ?></td>
					<td><?php echo $value->nombres ?></td>
					<td><?php echo $value->apellidos ?></td>
					<td><?php echo $value->correo ?></td>
					<td><?php echo $value->edad ?></td>
					<td><?php echo $value->genero ?></td>
					<td><?php echo $value->rh ?></td>
					<td><?php echo $value->programaFormacion ?></td>
					<td><form action="?c=Estudiantes&m=modificarEstudiante" method="post">
						<input type="hidden" name="consAprendiz" id="consAprendiz" value="<?php echo $value->consAprendiz ?>"><button class="btn btn-warning btn-sm">modificar</button></form></td>
					<td><form action="?c=Estudiantes&m=eliminarEstudiante" method="post">
						<input type="hidden" name="consAprendiz" id="consAprendiz" value="<?php echo $value->consAprendiz ?>"><button class="btn btn-danger btn-sm">eliminar</button></form></td>
				</tr>
				<?php
					}		
				 ?>
			</table>
			</div>
		</div>
	</div>
</div>