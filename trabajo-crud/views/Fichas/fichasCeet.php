<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h2>Reporte de Fichas de Aprendizaje</h2>
			<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<tr>
					<th>Ficha de Aprendizaje</th>
					<th>Programa de Formación</th>
				</tr>
				<?php 
				foreach ($this->modelFichas->consultarFichas() as $value) {
					?>
				<tr>
					<td><?php echo $value->fichaAprendizaje ?></td>
					<td><?php echo $value->programaFormacion ?></td>
					<td><form action="?c=Fichas&m=modificarFicha" method="post">
						<input type="hidden" name="fichaAprendizaje" id="fichaAprendizaje" value="<?php echo $value->fichaAprendizaje ?>"><button class="btn btn-warning btn-sm">Modificar</button></form></td>
					<td><form action="?c=Fichas&m=eliminarFichas" method="post">
						<input type="hidden" name="fichaAprendizaje" id="fichaAprendizaje" value="<?php echo $value->fichaAprendizaje ?>"><button class="btn btn-danger btn-sm">Eliminar</button></form></td>
				</tr>
				<?php
					}
				 ?>
			</table>
		</div>
		</div>
		<div class="col-md-4">
			<h2>Crear Fichas de aprendizaje</h2>
			<form action="?c=Fichas&m=crearFichas" method="post">
				<div class="form-group">
					<label for="fichas">Nombre del programa</label>
					<input type="text" name="programaFormacion" id="programaFormacion" class="form-control">
				</div>
				<hr>
				<center><button class="btn btn-success">Crear Ficha</button></center>
				<br>
			</form>
		</div>
	</div>
</div>