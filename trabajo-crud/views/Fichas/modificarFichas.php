<div class="container">
<div class="row">
	<h2>Modificar Fichas</h2>
		<div class="col-md-8">
			<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<tr>
					<th>Ficha de Aprendizaje</th>
					<th>Programa de Formacion</th>
				</tr>
				<?php 
				foreach ($this->modelFichas->consultarFichas() as $value) {
					if ($value->fichaAprendizaje == $fichaAprendizaje=$_POST['fichaAprendizaje']){
					?>
				<tr>
					<td><?php echo $value->fichaAprendizaje ?></td>
					<td><?php echo $value->programaFormacion ?></td>
				</tr>
			</table>
		</div>
		</div>
		<div class="col-md-4">
			<form action="?c=Fichas&m=modificarFichas" method="post">
				<div class="form-group">
					<label for="programaFormacion">Programa de Formación</label>
					<input type="text" name="programaFormacion" id="programaFormacion" class="form-control" value="<?php echo $value->programaFormacion ?>">
				</div>
				<div class="form-group">
					<label for="fichaAprendizaje"></label>
					<input type="hidden" name="fichaAprendizaje" id="fichaAprendizaje" class="form-control" value="<?php echo $value->fichaAprendizaje ?>">
				</div>
				<hr>
				<button class="btn btn-info">Actualizar</button>
			</form>
				<?php
					}
				}
					?>
		</div>
	</div>
</div>