<?php 
$conectar= new PDO('mysql:host=localhost;dbname=db_trabajo;charset=utf8','root','');

try {
	$stm = $conectar->prepare("SELECT tipoDoc FROM TipoDocument");
	$stm->execute();
	$tdoc = $stm->fetchAll();
} catch (PDOException $e) {
	die($e->getMessage());
}

try {
	$stm = $conectar->prepare("SELECT programaFormacion FROM Fichas");
	$stm->execute();
	$values = $stm->fetchAll();
} catch (PDOException $e) {
	die($e->getMessage());
}
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>Crear Estudiantes</h2>
			<hr>
			<form action="?c=Estudiantes&m=crearEstudiantes" method="post" >
				<div class="form-group">
					<label for="tipoDocumento">Tipo de documento</label>
						<select class="form-control" name="tipoDocumento" id="tipoDocumento" required>
							<option value="">Seleccione:</option>
							<?php foreach ($tdoc as $value){?>
							<option value="<?php echo $value["tipoDoc"]; ?>">
								<?php echo $value["tipoDoc"];?></option>
							<?php } ?>
						</select>
				</div>
				<div class="form-group">
					<label for="documento">Documento</label>
					<input type="number" name="documento" min="0" id="documento" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="nombres">Nombres</label>
					<input type="text" name="nombres" id="nombres" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="apellidos">Apellidos</label>
					<input type="text" name="apellidos" id="apellidos" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="correo">Correo</label>
					<input type="email" name="correo" id="correo" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="edad">Edad</label>
					<input type="number" name="edad" min="16" max="99" id="edad" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="genero">Genero</label>
					<select class="form-control" name="genero" id="genero" required>
						<option value="">Seleccione:</option>
						<option value="masculino">Hombre</option>
						<option value="femenino">Mujer</option>
						<option value="otro">Otro</option>
					</select>
				</div>
				<div class="form-group">
					<label for="RH">Tipo de Sangre</label>
					<select class="form-control" name="rh" id="rh" required>
						<option value="">Seleccione:</option>
						<option value="O +">O Positivo (+)</option>
						<option value="O -">O Negativo (-)</option>
						<option value="A +">A Positivo (+)</option>
						<option value="A -">A Negativo (-)</option>
						<option value="B +">B Positivo (+)</option>
						<option value="B -">B Negativo (-)</option>
						<option value="AB +">AB Positivo (+)</option>
						<option value="AB -">AB Negativo (-)</option>
					</select>
				</div>
				<div class="form-group">
					<label for="ficha">Ficha</label>
					<select class="form-control" name="ficha" id="ficha" required>
						<option value="">Seleccione:</option>
						<?php foreach ($values as $value){?>
							<option value="<?php echo $value["programaFormacion"]; ?>">
								<?php echo $value["programaFormacion"];?></option>
						<?php } ?>
					</select>
				</div>
				<hr>
				<center><button class="btn btn-success">Registrar</button></center>
				<br>
			</form>
		</div>
	</div>
</div>